import os
import shutil
import tkinter as tk
from tkinter import filedialog, ttk, simpledialog
import json
from datetime import datetime


class ErrorDialog(simpledialog.Dialog):
    def __init__(self, parent, title=None, message=None):
        self.message = message
        super().__init__(parent, title=title)

    def body(self, master):
        tk.Label(master, text=self.message).pack()
        return None


class SearchReplaceDialog(simpledialog.Dialog):
    def __init__(self, parent, text_widget, title=None):
        self.text_widget = text_widget
        super().__init__(parent, title=title)

    def body(self, master):
        self.search_var = tk.StringVar()
        self.replace_var = tk.StringVar()
        tk.Label(master, text="Search:").grid(row=0, sticky='w')
        self.search_entry = tk.Entry(master, textvariable=self.search_var)
        self.search_entry.grid(row=0, column=1)
        tk.Label(master, text="Replace:").grid(row=1, sticky='w')
        tk.Entry(master, textvariable=self.replace_var).grid(row=1, column=1)
        tk.Button(master, text="Search", command=self.search_text).grid(row=2, column=0, sticky='w')
        tk.Button(master, text="Replace", command=self.replace_text).grid(row=2, column=1, sticky='w')
        return self.search_entry  # return the widget that should have initial focus

    def buttonbox(self):
        # remove default buttons
        pass

    def search_text(self):
        text_widget = self.text_widget
        text_widget.tag_remove("highlight", "1.0", "end")
        search_term = self.search_var.get()
        if search_term:
            start = 1.0
            while True:
                pos = text_widget.search(search_term, start, stopindex=tk.END)
                if not pos:
                    break
                end = f"{pos} + {len(search_term)} chars"
                text_widget.tag_add("highlight", pos, end)
                start = end
            text_widget.tag_config("highlight", background="yellow", foreground="black")

    def replace_text(self):
        text_widget = self.text_widget
        search_term = self.search_var.get()
        replace_term = self.replace_var.get()
        if search_term is not None:
            while True:
                pos = text_widget.search(search_term, '1.0', stopindex=tk.END)
                if not pos:
                    break
                end = f"{pos} + {len(search_term)} chars"
                text_widget.delete(pos, end)
                text_widget.insert(pos, replace_term)


class FileCopyTool:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("File Copy Tool")
        self.root.geometry('800x600')
        self.root.minsize(800, 600)
        self.setup_widgets()

    def setup_widgets(self):
        self.root_path_button = tk.Button(self.root, text="Select Root Path", command=lambda: self.select_path(self.root_path_entry))
        self.root_path_button.grid(row=0, column=0, sticky='w', padx=5, pady=5)

        self.root_path_entry = tk.Entry(self.root, width=50, justify='left')
        self.root_path_entry.grid(row=0, column=1, sticky='we', padx=5, pady=5)

        self.target_path_button = tk.Button(self.root, text="Select Target Path", command=lambda: self.select_path(self.target_path_entry))
        self.target_path_button.grid(row=1, column=0, sticky='w', padx=5, pady=5)

        self.target_path_entry = tk.Entry(self.root, width=50, justify='left')
        self.target_path_entry.grid(row=1, column=1, sticky='we', padx=5, pady=5)

        self.relative_paths_text = tk.Text(self.root, height=10, width=50)
        self.relative_paths_text.grid(row=2, column=0, columnspan=2, sticky='nsew', padx=5, pady=5)
        self.relative_paths_text.bind("<<Selection>>", self.highlight_text)
        self.relative_paths_text.bind("<Double-Button-1>", self.select_word)
        self.relative_paths_text.bind("<Triple-Button-1>", self.select_line)

        self.progress = ttk.Progressbar(self.root, orient='horizontal', length=200, mode='determinate')
        self.progress.grid(row=3, column=0, columnspan=2, sticky='we', padx=5, pady=5)

        self.button_frame = tk.Frame(self.root)
        self.button_frame.grid(row=4, column=0, columnspan=2, sticky='we', padx=5, pady=5)

        self.import_button = tk.Button(self.button_frame, text="Import", command=self.import_files)
        self.import_button.grid(row=0, column=0, padx=5, pady=5, sticky='w')

        self.save_button = tk.Button(self.button_frame, text="Save", command=self.save_files)
        self.save_button.grid(row=0, column=1, padx=5, pady=5, sticky='w')

        self.format_button = tk.Button(self.button_frame, text="Format", command=self.format_text)
        self.format_button.grid(row=0, column=2, padx=5, pady=5, sticky='w')

        self.copy_button = tk.Button(self.button_frame, text="Copy Files", command=self.copy_files)
        self.copy_button.grid(row=0, column=3, padx=5, pady=5, sticky='e')

        self.relative_paths_text = tk.Text(self.root, height=10, width=50)
        self.relative_paths_text.grid(row=2, column=0, columnspan=2, sticky='nsew', padx=5, pady=5)
        self.relative_paths_text.bind("<<Selection>>", self.highlight_text)
        self.relative_paths_text.bind("<Double-Button-1>", self.select_word)
        self.relative_paths_text.bind("<Triple-Button-1>", self.select_line)
        self.relative_paths_text.bind("<Control-f>", self.open_search_replace_dialog)

        self.root.grid_columnconfigure(1, weight=1)
        self.root.grid_rowconfigure(2, weight=1)

    def run(self):
        self.root.mainloop()

    def open_search_replace_dialog(self, event):
        SearchReplaceDialog(self.root, self.relative_paths_text, title="Search & Replace")

    def highlight_text(self, event):
        text_widget = self.relative_paths_text
        text_widget.tag_remove("highlight", "1.0", "end")
        try:
            selected_text = text_widget.get("sel.first", "sel.last")
            start = 1.0
            while True:
                pos = text_widget.search(selected_text, start, stopindex=tk.END)
                if not pos:
                    break
                end = f"{pos} + {len(selected_text)} chars"
                text_widget.tag_add("highlight", pos, end)
                start = end
            text_widget.tag_config("highlight", background="yellow", foreground="black")
        except tk.TclError:
            pass

    def select_word(self, event):
        text_widget = self.relative_paths_text
        text_widget.tag_remove("highlight", "1.0", "end")
        line_index, char_index = text_widget.index(tk.CURRENT).split('.')
        line_text = text_widget.get(f"{line_index}.0", f"{line_index}.end")
        start_index = max(line_text.rfind('/', 0, int(char_index)), line_text.rfind('.', 0, int(char_index))) + 1
        end_index = min(line_text.find('/', int(char_index)), line_text.find('.', int(char_index)))
        if end_index == -1:
            end_index = len(line_text)
        text_widget.tag_add("sel", f"{line_index}.{start_index}", f"{line_index}.{end_index}")
        text_widget.tag_config("sel", background="yellow", foreground="black")
        return "break"

    def select_line(self, event):
        text_widget = self.relative_paths_text
        text_widget.tag_remove("highlight", "1.0", "end")
        line_index = text_widget.index(tk.CURRENT).split('.')[0]
        text_widget.tag_add("sel", f"{line_index}.0", f"{line_index}.end")
        text_widget.tag_config("sel", background="yellow", foreground="black")
        return "break"

    def copy_files(self):
        root_path = self.root_path_entry.get()
        target_path = self.target_path_entry.get()
        text_widget = self.relative_paths_text
        progress = self.progress
        if not root_path or not target_path:
            ErrorDialog(self.root, title="Error", message="Root path and target path cannot be empty.")
            return
        try:
            selected_text = text_widget.get("sel.first", "sel.last")
            relative_paths = [path for path in selected_text.split('\n') if path.strip()]
            selected = True
        except tk.TclError:
            relative_paths = [path for path in text_widget.get('1.0', 'end-1c').split('\n') if path.strip()]
            selected = False
        total_files = len(relative_paths)
        success_files = 0
        text_widget.tag_remove("error", "1.0", "end")
        text_widget.tag_remove("success", "1.0", "end")
        for i, relative_path in enumerate(relative_paths):
            start = f"1.0 + {i} lines"
            end = f"{start} lineend"
            try:
                source_path = os.path.join(root_path, relative_path)
                destination_path = os.path.join(target_path, relative_path)
                os.makedirs(os.path.dirname(destination_path), exist_ok=True)
                shutil.copy2(source_path, destination_path)
                success_files += 1
                progress['value'] = success_files / total_files * 100
                self.root.update_idletasks()
                source_dir = os.path.dirname(source_path)
                filename, ext = os.path.splitext(os.path.basename(source_path))
                for file in os.listdir(source_dir):
                    if file.startswith(filename) and '$' in file:
                        shutil.copy2(os.path.join(source_dir, file), os.path.join(os.path.dirname(destination_path), file))
                if selected:
                    start = f"sel.first linestart + {i} lines"
                    end = f"{start} lineend"
                text_widget.tag_add("success", start, end)
                text_widget.tag_config("success", foreground="green")
                print(f"Success occurred while copying {source_path} to {destination_path}")
            except Exception as e:
                print(f"Error occurred while copying {source_path} to {destination_path}: {str(e)}")
                if selected:
                    start = f"sel.first linestart + {i} lines"
                    end = f"{start} lineend"
                text_widget.tag_add("error", start, end)
                text_widget.tag_config("error", foreground="red")

    def select_path(self, entry):
        initial_dir = entry.get() if os.path.isdir(entry.get()) else "/"
        path = filedialog.askdirectory(initialdir=initial_dir)
        if path:
            entry.delete(0, tk.END)
            entry.insert(0, path)

    def save_files(self):
        file_paths = self.relative_paths_text.get('1.0', 'end-1c')
        root_path = self.root_path_entry.get()
        target_path = self.target_path_entry.get()
        current_time = datetime.now()
        formatted_time = current_time.strftime("%Y-%m-%d %H:%M:%S")
        file_paths = file_paths.split('\n')
        data = {
            "root_path": root_path,
            "target_path": target_path,
            "file_paths": file_paths,
            "save_time": formatted_time
        }
        file_path = filedialog.asksaveasfilename(defaultextension=".json", filetypes=[("JSON Files", "*.json")],
                                                 initialdir=target_path)
        if file_path:
            with open(file_path, "w", encoding='utf-8') as file:
                json.dump(data, file)

    def import_files(self):
        file_path = filedialog.askopenfilename(filetypes=[("JSON Files", "*.json")], initialdir=self.target_path_entry.get())
        if file_path:
            with open(file_path, "r", encoding='utf-8') as file:
                data = json.load(file)
                self.root_path_entry.delete(0, tk.END)
                self.root_path_entry.insert(0, data["root_path"])
                self.target_path_entry.delete(0, tk.END)
                self.target_path_entry.insert(0, data["target_path"])
                self.relative_paths_text.delete('1.0', tk.END)
                self.relative_paths_text.insert('1.0', '\n'.join(data["file_paths"]))

    def format_text(self):
        text = self.relative_paths_text.get('1.0', 'end-1c')
        lines = sorted(set(line.strip() for line in text.strip().split('\n') if line.strip()))
        self.relative_paths_text.delete('1.0', tk.END)
        self.relative_paths_text.insert('1.0', '\n'.join(lines))


if __name__ == "__main__":
    tool = FileCopyTool()
    tool.run()